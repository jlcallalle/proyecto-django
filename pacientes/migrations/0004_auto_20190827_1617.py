# Generated by Django 2.2.4 on 2019-08-27 16:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pacientes', '0003_auto_20190822_2155'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pacientes',
            old_name='numDoc',
            new_name='numero_documento',
        ),
        migrations.RenameField(
            model_name='pacientes',
            old_name='tipoDoc',
            new_name='tipo_documento',
        ),
    ]
