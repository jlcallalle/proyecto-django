""" Pacientes Application module"""

from django.apps import AppConfig


class PacientesConfig(AppConfig):
    """ Pacientes Application settings """
    name = 'pacientes'
    verbose_name = "Pacientes"
