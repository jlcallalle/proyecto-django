# Generated by Django 2.2.4 on 2019-08-22 21:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pacientes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pacientes',
            name='fecha',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='pacientes',
            name='tipoDoc',
            field=models.CharField(choices=[('DNI', 'DNI'), ('CE', 'Carné de Extranjería'), ('IN', 'Indocumentado')], default='DNI', max_length=3),
        ),
    ]
