# Generated by Django 2.2.4 on 2019-08-22 21:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pacientes', '0002_auto_20190822_2142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pacientes',
            name='numDoc',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
