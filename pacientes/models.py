from django.db import models
from datetime import datetime,date
from dateutil.relativedelta import relativedelta

# Create your models here.

from django.db import models
class Pacientes(models.Model):
    tipo_documento = models.CharField(max_length=3, choices=[
        ('DNI', 'DNI'),
        ('CE', 'Carné de Extranjería'),
        ('IN', 'Indocumentado'),
    ], default='DNI')
    numero_documento = models.CharField(max_length=100, unique=True)
    nombres = models.TextField()
    appPaterno = models.CharField(max_length=100)
    appMaterno = models.CharField(max_length=100)
    fecha = models.DateField()
    
    def edad(self):
        edad1=relativedelta(datetime.now().date(), self.fecha)
        if edad1.years < 1:
            if edad1.months < 0:
                resultado = '{day} días'.format(day=edad1.days)
            else:
                resultado = '{month} meses {day} días'.format(month=edad1.months,day=edad1.days)
        else:
            resultado = '{year} años, {month} meses {day} días '.format(year=edad1.years, month=edad1.months, day=edad1.days)

        return resultado

    @classmethod
    def get_pacientes_por_documento(cls, tipo, numero):
        try:
            paciente = [Paciente.objects.get(tipo_de_documento=tipo,
                                             numero_de_documento=numero)]
        except Paciente.DoesNotExist:
            paciente = Paciente.objects.all()
        return paciente
