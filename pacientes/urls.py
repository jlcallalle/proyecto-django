from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.ListarPacientes),
    path('crear/', views.CrearPaciente),
    path('editar/<int:pk>', views.EditarPaciente),
    path('eliminar/<int:pk>', views.EliminarPaciente),
]