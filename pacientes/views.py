from django.shortcuts import render, get_object_or_404, redirect
from .models import Pacientes
from .form import CrearPacienteForm, BuscarPacienteForm


# Create your views here.
def index(request):
    return render(request, 'index.html', {})




def ListarPacientes(request):
    pacientes = Pacientes.objects.all()
    form = BuscarPacienteForm()

    if request.method=='POST':
        try:
            pacientes=[Pacientes.objects.get(
                tipo_documento=request.POST.get('tipo_documento'),
                numero_documento=request.POST.get('numero_documento')
            )]
        except Pacientes.DoesNotExist:
            print('hola')

    return render(request, "listar-paciente.html", {'pacientes': pacientes, 'form': form})



def CrearPaciente(request):
    if request.method == 'POST':
        form = CrearPacienteForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = CrearPacienteForm()
    return render(request, 'crear-paciente.html', {'form': form})

def EditarPaciente(request, pk):
    pacientes = get_object_or_404(Pacientes, pk=pk)
    if request.method == 'POST':
        form = CrearPacienteForm(request.POST, instance=pacientes)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = CrearPacienteForm(instance=pacientes)

    return render(request, 'editar_paciente.html', {'form': form})

def EliminarPaciente(request, pk):
    pacientes = Pacientes.objects.get(pk=pk)
    if request.method == 'POST':
        pacientes.delete()
        return redirect('/')
    return render(request, 'eliminar_paciente.html', {'pacientes': pacientes})
