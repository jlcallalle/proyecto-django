from django.views.generic import  ListView
from django import forms
from django.shortcuts import render
from django.forms import models, DateInput
from .models import Pacientes

class CrearPacienteForm(forms.models.ModelForm):
    class Meta:
        model = Pacientes
        fields = '__all__'
        widgets = {
                'fechaDeNacimiento': forms.DateInput(attrs={'type':'date'})
                }
        labels = {
                'tipo_documento' : 'Tipo de Documento',
                'numero_documento' : 'Número de Documento',
                'nombres' : 'Nombres',
                'appPaterno' : 'Apellido Paterno',
                'appMaterno': 'Apellido Materno',
                'fecha' : 'Fecha Nacimiento',
                }
class BuscarPacienteForm(models.ModelForm):
    class Meta:
        model = Pacientes
        fields = ('tipo_documento', 'numero_documento')
        labels = {'tipo_documento': 'Tipo de Documento',
                  'numero_documento': 'Numero de Documento'}
