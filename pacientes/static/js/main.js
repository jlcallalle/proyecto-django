// Main

var MyApp = {

  obtenerEdad : function() {
    var ListaFechas =  document.querySelectorAll('.table .fechal');
    for (var i = 0 ; i < ListaFechas.length; ++i) {
      var anio = ListaFechas[i].textContent.split(" ")[2];
      var edad = 2019 - anio;
      console.log(anio);
    }

    var ListaFechas =  document.querySelectorAll('.table .edadl');
    for (var i = 0 ; i < ListaFechas.length; ++i) {
     ListaFechas[i].textContent = edad;
    }
  },
  edadEditar : function () {
      var cabecera = document.getElementById('id_fecha');
      var anio = cabecera.value.slice(0,4);
      console.log(anio);
      var inputEdad = document.getElementById('inputEdad');
      var edad = 2019 - anio;
      inputEdad.value = edad;
  },
}

$(function () {
  $("#id_fecha").datepicker();
  $("#id_fecha").attr("autocomplete", "off");

  if ($('.section-editar').length) {
      MyApp.edadEditar();
  }

   if ($('.crear-paciente').length) {

    var fecha = document.getElementById("id_fecha");
    fecha.addEventListener("blur", myBlurFunction, true);

    function myBlurFunction() {
      var cabecera = document.getElementById('id_fecha');
      var anio = cabecera.value.slice(6,10);
      var edadFinal = 2019 - anio;
      var inputEdad = document.getElementById('inputEdad');
      inputEdad.value = edadFinal;
    }

  }

});
